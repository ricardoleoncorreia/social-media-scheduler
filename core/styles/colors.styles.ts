export const globalColors = {
  orange: '#f7931a',
  softOrange: '#ffe9d5',
  blue: '#1a9af7',
  softBlue: '#e7f5ff',
  black: '#201e1c',
  warmBlack: '#282623',
  grey: '#bababa',
  darkGrey: '#808080',
  offWhite: '#faf8f7',
  justWhite: '#ffffff',
};
