import { createGlobalStyle } from 'styled-components';
import { homepageCommonStyles } from '@/components/Homepage/Homepage.styles';

const smallBreakpoint = `860px`;
const mediumBreakpoint = `1280px`;

export const GlobalResponsiveVariables = createGlobalStyle`
  :root {
    @media screen and (max-width: ${smallBreakpoint}) {
      --homepage__statistic__summary: wrap;
      --homepage__statistic__padding: 40px 20px;
      --homepage__join-today__height: 50vh;
      --homepage__benefits__grid-columns: 1;
      --homepage__benefits__grid-rows: 4;
      --homepage__benefits__grid-container__width: 100%;

      --footer__container__flex-direction: column;
      --footer__legal__align-items: center;
      --footer__rights__align-items: center;

      --policy__container__h-padding: 10vw;
    }

    @media screen and (min-width: ${smallBreakpoint}) and (max-width: ${mediumBreakpoint}) {
      --homepage__statistic__summary: nowrap;
      --homepage__statistic__padding: 80px 20px;
      --homepage__join-today__height: 60vh;
      --homepage__benefits__grid-columns: 2;
      --homepage__benefits__grid-rows: 2;
      --homepage__benefits__grid-container__width: ${homepageCommonStyles.maxGridContainerWidth};

      --footer__container__flex-direction: row;
      --footer__legal__align-items: flex-start;
      --footer__rights__align-items: flex-end;

      --policy__container__h-padding: 20vw;
    }

    @media screen and (min-width: ${mediumBreakpoint}) {
      --homepage__statistic__summary: nowrap;
      --homepage__statistic__padding: 80px 20px;
      --homepage__join-today__height: 60vh;
      --homepage__benefits__grid-columns: 2;
      --homepage__benefits__grid-rows: 2;
      --homepage__benefits__grid-container__width: ${homepageCommonStyles.maxGridContainerWidth};

      --footer__container__flex-direction: row;
      --footer__legal__align-items: flex-start;
      --footer__rights__align-items: flex-end;

      --policy__container__h-padding: auto;
    }
  }
`;
