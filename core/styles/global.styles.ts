import { createGlobalStyle } from 'styled-components';
import { globalColors } from './colors.styles';

export const GlobalStyles = createGlobalStyle`
  html {
    font-size: 12px;
    scroll-behavior: smooth;
  }

  body {
    margin: 0;
    font-family: 'DM Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background-color: ${globalColors.offWhite};
    color: ${globalColors.warmBlack};
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
  }

  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  #__next {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-height: 100vh;
  }
`;
