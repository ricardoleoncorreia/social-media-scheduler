import React from 'react';
import { TestMocks } from '@/shared/mocks/test.mocks';
import { StateActions, stateReducer } from './state.reducer';

const { username, signedInState, signedOutState } = TestMocks.state;
window.console.error = jest.fn();

/**
 * @jest-environment jsdom
 */
// TODO: needs to be removed once a notification system is implemented
describe('StateReducer', () => {
  describe('when a sing in action is dispatched', () => {
    const actionType = StateActions.SignIn;

    describe(`and has the username "${username}"`, () => {
      const action = {
        type: actionType,
        payload: { username },
      };

      test('should allow the user to sign in', () => {
        expect(signedOutState.isSignedIn).toBeFalsy();
        const newState = stateReducer(signedOutState, action);
        expect(newState.isSignedIn).toBeTruthy();
      });

      test('should return the username', () => {
        expect(signedOutState.username).toBeFalsy();
        const newState = stateReducer(signedOutState, action);
        expect(newState.username).toBe(username);
      });
    });

    describe(`and has NO username`, () => {
      const action = {
        type: actionType,
      };

      test('should NOT allow the user to sign in', () => {
        expect(signedOutState.isSignedIn).toBeFalsy();
        const newState = stateReducer(signedOutState, action);
        expect(newState.isSignedIn).toBeFalsy();
        expect(newState.username).toBeUndefined();
      });

      test('should return the previous state', () => {
        const newState = stateReducer(signedOutState, action);
        expect(newState).toBe(signedOutState);
      });
    });
  });

  describe('when a sing out action is dispatched', () => {
    const action = {
      type: StateActions.SignOut,
      payload: { username },
    };

    test('should sign out', () => {
      expect(signedInState.isSignedIn).toBeTruthy();
      const newState = stateReducer(signedInState, action);
      expect(newState.isSignedIn).toBeFalsy();
    });

    test('should remove the username', () => {
      expect(signedInState.username).toBe(username);
      const newState = stateReducer(signedInState, action);
      expect(newState.username).toBeUndefined();
    });
  });

  describe(`when an action type that doesn't exist is provided`, () => {
    const action = {
      type: 'actionTypeThatDoesNotExists',
    };

    test('should return the previous state', () => {
      const newState = stateReducer(signedOutState, action);
      expect(newState).toBe(signedOutState);
    });
  });
});
