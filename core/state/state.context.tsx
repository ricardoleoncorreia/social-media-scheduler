import React, { createContext, ReactElement, useContext, useReducer } from 'react';
import { IStateContext } from './state.namespace';
import { initialGlobalState } from './state.reducer';

type ContextType = [IStateContext.State, React.Dispatch<IStateContext.Action>];
const noop = () => {
  /* NOOP */
};
const StateContext = createContext<ContextType>([initialGlobalState, noop]);

export const GlobalStateProvider = ({ reducer, initialState, children }: IStateContext.ProviderProps): ReactElement => (
  <StateContext.Provider value={useReducer(reducer, initialState)}>{children}</StateContext.Provider>
);

export const useGlobalState = (): ContextType => useContext(StateContext);
