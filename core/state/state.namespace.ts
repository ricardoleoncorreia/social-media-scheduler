export declare namespace IStateContext {
  export interface State {
    isSignedIn: boolean;
    username?: string;
  }

  export interface Action {
    type: string;
    payload?: Partial<State>;
  }

  export type Reducer = (state: State, action: Action) => State;

  export interface ProviderProps {
    reducer: Reducer;
    initialState: State;
    children: React.ReactNode;
  }
}
