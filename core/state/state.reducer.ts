import { IStateContext } from './state.namespace';

export const initialGlobalState: IStateContext.State = {
  isSignedIn: false,
};

export enum StateActions {
  SignIn = 'SignIn',
  SignOut = 'SignOut',
}

export const stateReducer: IStateContext.Reducer = (state, action): IStateContext.State => {
  try {
    switch (action.type) {
      case StateActions.SignIn:
        if (!action.payload?.username) throw Error('No username was provided for sign in reducer action');
        return { ...state, isSignedIn: true, username: action.payload.username };
      case StateActions.SignOut:
        return { ...state, isSignedIn: false, username: undefined };
      default:
        return state;
    }
  } catch (error) {
    console.error(error);
    return state;
  }
};
