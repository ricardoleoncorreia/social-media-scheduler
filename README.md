# Social Media Scheduler

This project is a minimum viable product (MVP) that allows users to schedule social media posts.

**Main technologies:** ReactJS, Typescript, Sass, Jest.

To see more details about the architecture of the application, visit the project
[documentation](https://gitlab.com/ricardoleoncorreia/social-media-scheduler/-/wikis/home).

## Installation process

1. Clone the [repo](https://gitlab.com/ricardoleoncorreia/social-media-scheduler.git) in your local machine.
1. Run `npm install` to install all required dependencies.
1. Run `npm start` to display the React client in your web browser.
