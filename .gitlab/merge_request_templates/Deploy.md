## Related issues

#ticket_number

## Conformity

- [ ] Update project version.
- [ ] Create tag for new version.
- [ ] [Documentation](https://gitlab.com/ricardoleoncorreia/social-media-scheduler/-/wikis/home).

/assign @username
/milestone %milestone
