## Related issues

#ticket_number

## What changed?

Brief summary of bug fixes or new functionality

## Notes

Particular considerations to take into account

## Conformity

- [ ] Added unit tests.
- [ ] Manually tested in Google Chrome & Mozilla Firefox.
- [ ] Endpoint added to Swagger.
- [ ] Endpoint tested with an API client.
- [ ] [Documentation](https://gitlab.com/ricardoleoncorreia/social-media-scheduler/-/wikis/home).

-OR-

No applicable conformity items for this merge request.

/label ~bug
/assign @username
/milestone %milestone
