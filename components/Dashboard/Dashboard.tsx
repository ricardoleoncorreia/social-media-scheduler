import React from 'react';
import PageNotImplemented from '@/shared/components/PageNotImplemented/PageNotImplemented';

export default function Dashboard(): JSX.Element {
  return <PageNotImplemented />;
}
