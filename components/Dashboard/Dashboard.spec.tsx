import React from 'react';
import { render, screen } from '@testing-library/react';
import Dashboard from './Dashboard';

const PageNotImplementedMock = () => <div>PageNotImplementedMock</div>;
jest.mock('@/shared/components/PageNotImplemented/PageNotImplemented', () => PageNotImplementedMock);

/**
 * @jest-environment jsdom
 */
describe('<Dashboard />', () => {
  beforeEach(() => {
    render(<Dashboard />);
  });

  test('should contains the work-in-progress message', () => {
    const wipMessage = screen.getByText('PageNotImplementedMock');
    expect(wipMessage).toBeInTheDocument();
  });
});
