import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithState } from '@/shared/testing/renderWithState';
import { PlaceHolderPageTestingBaseProps } from '../../shared/components/PlaceholderPage/PlaceholderPage.props';
import PageNotFound from './PageNotFound';

const runTests = ({ pageName, isSignedIn }: PlaceHolderPageTestingBaseProps) => {
  beforeEach(() => {
    renderWithState({
      isSignedIn,
      children: <PageNotFound />,
    });
  });

  test(`should show a "404" title`, () => {
    const title = screen.getByText(`404`);
    expect(title).toBeInTheDocument();
  });

  test(`should show a description`, () => {
    const description = screen.getByText(`Page Not Found`);
    expect(description).toBeInTheDocument();
  });

  xdescribe('and user click on the button', () => {
    test(`should redirect to the ${pageName.toLowerCase()}`, () => {
      const button = screen.getByText(`Go to ${pageName}`);

      userEvent.click(button, { button: 0 });

      // expect(history.location.pathname).toBe(path);
    });
  });
};

describe('<PageNotFound />', () => {
  describe('when user has signed in', () => {
    runTests({
      pageName: 'Dashboard',
      path: '/dashboard',
      isSignedIn: true,
    });
  });

  describe('when user has signed out', () => {
    runTests({
      pageName: 'Homepage',
      path: '/',
      isSignedIn: false,
    });
  });
});
