import React from 'react';
import PlaceholderPage from '../../shared/components/PlaceholderPage/PlaceholderPage';

export default function PageNotFound(): JSX.Element {
  return <PlaceholderPage icon='404' message='Page Not Found' />;
}
