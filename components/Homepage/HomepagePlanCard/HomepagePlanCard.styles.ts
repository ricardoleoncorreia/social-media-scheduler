import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';

export const HomepagePlanCardContainer = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  width: 70%;
  min-width: 220px;
  max-width: 300px;
  height: 250px;
  border-radius: 15px;
  position: relative;
  margin: 20px 5px;
  padding: 20px;
  box-shadow: 0 4px 8px rgba(40 38 35 / 25%);
  scroll-snap-align: center;
`;

export const HomepagePlanCardRecommendedSign = styled.p`
  position: absolute;
  left: 50%;
  top: 0;
  padding: 8px 12px;
  border-radius: 8px;
  transform: translate(-50%, -50%);
  background-color: ${globalColors.orange};
  color: ${globalColors.justWhite};
  font-weight: bold;
`;

export const HomepagePlanCardTitle = styled.p`
  margin-top: 10px !important;
  margin-bottom: 0;
  font-weight: bold;
  font-size: 1.2rem;
  text-align: center;
`;

export const HomepagePlanCardAmount = styled.p`
  position: relative;
  margin: 0 auto;
  font-size: 5.2rem;
  font-weight: bold;
  line-height: 5.2rem;

  &::before {
    content: '$';
    position: absolute;
    top: -1rem;
    left: -1rem;
    height: 1.6rem;
    font-size: 1.6rem;
    font-weight: bold;
  }
`;

export const HomepagePlanCardDescription = styled.p`
  margin: 0 auto;
  text-align: center;
  font-weight: bold;
  color: ${globalColors.darkGrey};
`;
