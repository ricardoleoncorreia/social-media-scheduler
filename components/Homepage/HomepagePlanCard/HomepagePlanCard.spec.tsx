import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import HomepagePlanCard from './HomepagePlanCard';
import { IHomepage } from '../Homepage.namespace';

// TODO: Implement cases that involve user events
describe('<HomepagePlanCard />', () => {
  const baseMockPlan: IHomepage.Plan = {
    id: 'id',
    title: 'title',
    amount: 19,
    description: 'description',
  };

  describe('when the plan is recommended', () => {
    const mockPlan: IHomepage.Plan = {
      ...baseMockPlan,
      isRecommended: true,
    };

    beforeEach(() => {
      render(<HomepagePlanCard plan={mockPlan} />);
    });

    test('should have a recommended sign', () => {
      const sign = screen.getByText(/recommended/i);
      expect(sign).toBeInTheDocument();
    });

    test('should have a title', () => {
      const title = screen.getByText(/title/i);
      expect(title).toBeInTheDocument();
    });

    test('should have a description', () => {
      const description = screen.getByText(/description/i);
      expect(description).toBeInTheDocument();
    });

    test('should show the price', () => {
      const price = screen.getByText(/19/i);
      expect(price).toBeInTheDocument();
    });

    test('should contain a button to go to the plan details', () => {
      const chooseText = screen.getByText(/choose/i);
      expect(chooseText).toBeInTheDocument();
    });

    xdescribe('and the user clicks on the choose button', () => {
      test('should take the user to the plans page', () => {
        const chooseElement = screen.getByText(/choose/i);

        userEvent.click(chooseElement, { button: 0 });

        // expect(history.location.pathname).toBe('/plan');
      });
    });
  });
});
