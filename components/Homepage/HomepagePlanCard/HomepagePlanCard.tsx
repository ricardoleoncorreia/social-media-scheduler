import React from 'react';
import { IHomepage } from '../Homepage.namespace';
import {
  HomepagePlanCardAmount,
  HomepagePlanCardContainer,
  HomepagePlanCardDescription,
  HomepagePlanCardRecommendedSign,
  HomepagePlanCardTitle,
} from './HomepagePlanCard.styles';
import { GhostLink } from '@/shared/components/RouterLinks/GhostLink';

export default function HomepagePlanCard(prop: IHomepage.Props.PlanCard): JSX.Element {
  return (
    <HomepagePlanCardContainer>
      {prop.plan.isRecommended && <HomepagePlanCardRecommendedSign>Recommended</HomepagePlanCardRecommendedSign>}
      <HomepagePlanCardTitle>{prop.plan.title}</HomepagePlanCardTitle>
      <HomepagePlanCardAmount>{prop.plan.amount}</HomepagePlanCardAmount>
      <HomepagePlanCardDescription>{prop.plan.description}</HomepagePlanCardDescription>
      <GhostLink path='/plan' content='Choose' />
    </HomepagePlanCardContainer>
  );
}
