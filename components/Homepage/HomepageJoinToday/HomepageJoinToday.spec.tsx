import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepageJoinToday from './HomepageJoinToday';
import { addBasePath } from '@/shared/functions/addBasePath';

describe('<HomepageJoinToday />', () => {
  beforeEach(() => {
    render(<HomepageJoinToday />);
  });

  test('should contain a title', () => {
    const joinTodayText = screen.getByText(/join today!/i);
    expect(joinTodayText).toBeInTheDocument();
  });

  test('should have a background image', () => {
    const joinTodaySection = screen.getByTestId('join-today-section');
    expect(joinTodaySection).toHaveStyle(`background-image: url(${addBasePath('/static/img/join-today.png')})`);
  });
});
