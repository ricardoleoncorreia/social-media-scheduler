import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { addBasePath } from '@/shared/functions/addBasePath';

export const HomepageJoinTodayContainer = styled.section`
  width: 100%;
  height: var(--homepage__join-today__height);
  position: relative;
  background-image: url(${addBasePath('/static/img/join-today.png')});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`;

export const HomepageJoinTodayLayer = styled.div`
  background-color: ${globalColors.orange};
  opacity: 0.875;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export const HomepageJoinTodayTitle = styled.h3`
  width: 100%;
  position: absolute;
  z-index: 100;
  margin-top: 13vh;
  color: ${globalColors.justWhite};
  text-align: center;
  font-size: 1.8rem;
`;
