import React from 'react';
import { HomepageJoinTodayContainer, HomepageJoinTodayLayer, HomepageJoinTodayTitle } from './HomepageJoinToday.styles';

export default function HomepageJoinToday(): JSX.Element {
  return (
    <HomepageJoinTodayContainer data-testid='join-today-section'>
      <HomepageJoinTodayLayer></HomepageJoinTodayLayer>
      <HomepageJoinTodayTitle>Join today!</HomepageJoinTodayTitle>
    </HomepageJoinTodayContainer>
  );
}
