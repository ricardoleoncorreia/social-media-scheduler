import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepageBenefits from './HomepageBenefits';

const HomepageBenefitCardMock = () => <div>HomepageBenefitCardMock</div>;
jest.mock('../HomepageBenefitCard/HomepageBenefitCard', () => HomepageBenefitCardMock);

describe('<HomepageBenefits />', () => {
  beforeEach(() => {
    render(<HomepageBenefits />);
  });

  test('should contain 4 benefit cards', () => {
    const benefitCards = screen.getAllByText('HomepageBenefitCardMock');
    expect(benefitCards).toHaveLength(4);
  });
});
