import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { homepageCommonStyles, homepageSectionStyles } from '../Homepage.styles';

export const HomepageBenefitsContainer = styled.section`
  ${homepageSectionStyles}
  background-color: ${globalColors.black};
`;

export const HomepageBenefitsGrid = styled.section`
  display: grid;
  grid-gap: ${homepageCommonStyles.gridGap};
  grid-template-columns: repeat(var(--homepage__benefits__grid-columns), auto);
  grid-template-rows: repeat(var(--homepage__benefits__grid-columns), auto);
  width: var(--homepage__benefits__grid-container__width);
  margin: auto;
`;
