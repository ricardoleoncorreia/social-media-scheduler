import React from 'react';
import { Constants } from '@/shared/constants.mock';
import { IHomepage } from '../Homepage.namespace';
import HomepageBenefitCard from '../HomepageBenefitCard/HomepageBenefitCard';
import { HomepageBenefitsContainer, HomepageBenefitsGrid } from './HomepageBenefits.styles';

export default function HomepageBenefits(): JSX.Element {
  return (
    <HomepageBenefitsContainer>
      <HomepageBenefitsGrid>
        {Constants.homepage.benefits &&
          Constants.homepage.benefits.map((benefit: IHomepage.Benefit) => <HomepageBenefitCard key={benefit.id} benefit={benefit} />)}
      </HomepageBenefitsGrid>
    </HomepageBenefitsContainer>
  );
}
