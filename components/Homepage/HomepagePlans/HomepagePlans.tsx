import React from 'react';
import { Constants } from '@/shared/constants.mock';
import { IHomepage } from '../Homepage.namespace';
import HomepagePlanCard from '../HomepagePlanCard/HomepagePlanCard';
import { HomepagePlansCarousel, HomepagePlansContainer, HomepagePlansDescription, HomepagePlansTitle } from './HomepagePlans.styles';

export default function HomepagePlans(): JSX.Element {
  return (
    <HomepagePlansContainer id='plans'>
      <HomepagePlansTitle>Choose the best plan that fits you.</HomepagePlansTitle>
      <HomepagePlansDescription>All our plans give you access to all of our features</HomepagePlansDescription>
      <HomepagePlansCarousel>
        {Constants.homepage.plans && Constants.homepage.plans.map((plan: IHomepage.Plan) => <HomepagePlanCard key={plan.id} plan={plan} />)}
      </HomepagePlansCarousel>
    </HomepagePlansContainer>
  );
}
