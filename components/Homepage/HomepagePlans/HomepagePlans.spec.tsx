import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepagePlans from './HomepagePlans';

const HomepagePlanCardMock = () => <div>HomepagePlanCardMock</div>;
jest.mock('../HomepagePlanCard/HomepagePlanCard', () => HomepagePlanCardMock);

describe('when the user is in the plans section', () => {
  const title = `Choose the best plan that fits you.`;
  const description = `All our plans give you access to all of our features`;

  beforeEach(() => {
    render(<HomepagePlans />);
  });

  test('should have a title', () => {
    const titleElement = screen.getByText(title);
    expect(titleElement).toBeInTheDocument();
  });

  test('should have a description', () => {
    const descriptionElement = screen.getByText(description);
    expect(descriptionElement).toBeInTheDocument();
  });

  test('should contain 3 plan cards', () => {
    const planCards = screen.getAllByText('HomepagePlanCardMock');
    expect(planCards).toHaveLength(3);
  });
});
