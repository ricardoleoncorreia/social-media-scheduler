import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { flexCenterContent } from '@/shared/styles/layout.styles';
import { homepageDetailsStyles, homepageSectionStyles, homepageTitleStyles } from '../Homepage.styles';

export const HomepagePlansContainer = styled.section`
  ${homepageSectionStyles}
  background-color: ${globalColors.offWhite};
`;

export const HomepagePlansTitle = styled.h3`
  ${homepageTitleStyles}
`;

export const HomepagePlansDescription = styled.p`
  ${homepageDetailsStyles}
  font-weight: bold;
  color: ${globalColors.darkGrey};
`;

export const HomepagePlansCarousel = styled.section`
  ${flexCenterContent}
  justify-content: start;
  max-width: 100vw;
  overflow-x: auto;
  overscroll-behavior-x: contain;
  scroll-snap-type: x proximity;
`;
