import { HomepageBenefitCardIcons } from './HomepageBenefitCard/HomepageBenefitCard.namespace';

export namespace IHomepage {
  export interface Statistic {
    id: string;
    label: string;
    value: number;
  }

  export interface Benefit {
    id: string;
    icon: HomepageBenefitCardIcons;
    title: string;
    description: string;
  }

  export interface Plan {
    id: string;
    isRecommended?: boolean;
    title: string;
    amount: number;
    description: string;
  }

  export namespace Props {
    export interface BenefitCard {
      benefit: Benefit;
    }

    export interface PlanCard {
      plan: Plan;
    }
  }
}
