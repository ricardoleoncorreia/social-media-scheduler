import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { flexCenterContent } from '@/shared/styles/layout.styles';
import { homepageDetailsStyles, homepageSectionStyles, homepageTitleStyles } from '../Homepage.styles';

export const HomepageStatisticsContainer = styled.section`
  ${homepageSectionStyles}
  background-color: ${globalColors.offWhite};
`;

export const HomepageStatisticsImage = styled.img`
  width: 90%;
  max-width: 500px;
  max-height: 312px;
  margin-top: 30px;
`;

export const HomepageStatisticsTitle = styled.h2`
  ${homepageTitleStyles}
`;

export const HomepageStatisticsDetails = styled.p`
  ${homepageDetailsStyles}
`;

export const HomepageStatisticsSummary = styled.section`
  ${flexCenterContent}
  flex-wrap: var(--homepage__statistic__summary);
`;

export const HomepageStatisticsSummaryItem = styled.div`
  ${flexCenterContent}
  flex-direction: column;
  margin: 20px;
`;

export const HomepageStatisticsSummaryItemValue = styled.span`
  font-size: 3rem;
`;

export const HomepageStatisticsSummaryItemLabel = styled.h3`
  font-size: 1.2rem;
`;
