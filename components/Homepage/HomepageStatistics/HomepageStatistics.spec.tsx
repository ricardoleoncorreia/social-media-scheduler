import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepageStatistics from './HomepageStatistics';

describe('<HomepageStatistics />', () => {
  const title = `Connect to the most relevant social networks`;
  const description = `Create and schedule, two simple steps to deliver your ads to your followers`;
  const testStatistics = (stats: string[]) => {
    stats.forEach((stat) => {
      test(`should contain a ${stat} count`, () => {
        const socialNetworksStats = screen.getByTestId(stat);
        expect(socialNetworksStats).toBeInTheDocument();

        const socialNetworksStatsValue = screen.getByTestId(`${stat}-value`);
        expect(socialNetworksStatsValue).toBeInTheDocument();

        const socialNetworksStatsLabel = screen.getByTestId(`${stat}-label`);
        expect(socialNetworksStatsLabel).toBeInTheDocument();
      });
    });
  };

  beforeEach(() => {
    render(<HomepageStatistics />);
  });

  test('should have a social media image', () => {
    const statsImage = screen.getByAltText(/social media/i);
    expect(statsImage).toBeInTheDocument();
  });

  test('should have a title', () => {
    const titleElement = screen.getByText(title);
    expect(titleElement).toBeInTheDocument();
  });

  test('should have a description', () => {
    const descriptionElement = screen.getByText(description);
    expect(descriptionElement).toBeInTheDocument();
  });

  testStatistics(['social-networks', 'posts', 'members']);
});
