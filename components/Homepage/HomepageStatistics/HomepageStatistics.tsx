import React from 'react';
import { Constants } from '@/shared/constants.mock';
import { addBasePath } from '@/shared/functions/addBasePath';
import { IHomepage } from '../Homepage.namespace';
import {
  HomepageStatisticsContainer,
  HomepageStatisticsDetails,
  HomepageStatisticsImage,
  HomepageStatisticsSummary,
  HomepageStatisticsSummaryItem,
  HomepageStatisticsSummaryItemLabel,
  HomepageStatisticsSummaryItemValue,
  HomepageStatisticsTitle,
} from './HomepageStatistics.styles';

export default function HomepageStatistics(): JSX.Element {
  return (
    <HomepageStatisticsContainer>
      <HomepageStatisticsImage src={addBasePath('/static/img/social-media.png')} alt='Social Media' />
      <HomepageStatisticsTitle>Connect to the most relevant social networks</HomepageStatisticsTitle>
      <HomepageStatisticsDetails>Create and schedule, two simple steps to deliver your ads to your followers</HomepageStatisticsDetails>
      <HomepageStatisticsSummary>
        {Constants.homepage.statistics &&
          Constants.homepage.statistics.map((statistic: IHomepage.Statistic) => {
            return (
              <HomepageStatisticsSummaryItem key={statistic.id} data-testid={statistic.id}>
                <HomepageStatisticsSummaryItemValue data-testid={`${statistic.id}-value`}>
                  {statistic.value}
                </HomepageStatisticsSummaryItemValue>
                <HomepageStatisticsSummaryItemLabel data-testid={`${statistic.id}-label`}>
                  {statistic.label}
                </HomepageStatisticsSummaryItemLabel>
              </HomepageStatisticsSummaryItem>
            );
          })}
      </HomepageStatisticsSummary>
    </HomepageStatisticsContainer>
  );
}
