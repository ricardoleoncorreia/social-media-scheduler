import React from 'react';
import { render, screen } from '@testing-library/react';
import Homepage from './Homepage';

const HomepageIntroMock = () => <div>HomepageIntroMock</div>;
jest.mock('./HomepageIntro/HomepageIntro', () => HomepageIntroMock);

const HomepageStatisticsMock = () => <div>HomepageStatisticsMock</div>;
jest.mock('./HomepageStatistics/HomepageStatistics', () => HomepageStatisticsMock);

const HomepageBenefitsMock = () => <div>HomepageBenefitsMock</div>;
jest.mock('./HomepageBenefits/HomepageBenefits', () => HomepageBenefitsMock);

const HomepageJoinTodayMock = () => <div>HomepageJoinTodayMock</div>;
jest.mock('./HomepageJoinToday/HomepageJoinToday', () => HomepageJoinTodayMock);

const HomepagePlansMock = () => <div>HomepagePlansMock</div>;
jest.mock('./HomepagePlans/HomepagePlans', () => HomepagePlansMock);

describe('<Homepage />', () => {
  beforeEach(() => {
    render(<Homepage />);
  });

  test('should have an intro section', () => {
    const introSection = screen.getByText('HomepageIntroMock');
    expect(introSection).toBeInTheDocument();
  });

  test('should have a statistics section', () => {
    const statisticsSection = screen.getByText('HomepageStatisticsMock');
    expect(statisticsSection).toBeInTheDocument();
  });

  test('should have a benefits section', () => {
    const benefitsSection = screen.getByText('HomepageBenefitsMock');
    expect(benefitsSection).toBeInTheDocument();
  });

  test('should have a join today section', () => {
    const joinTodaySection = screen.getByText('HomepageJoinTodayMock');
    expect(joinTodaySection).toBeInTheDocument();
  });

  test('should have a plans section', () => {
    const plansSection = screen.getByText('HomepageJoinTodayMock');
    expect(plansSection).toBeInTheDocument();
  });
});
