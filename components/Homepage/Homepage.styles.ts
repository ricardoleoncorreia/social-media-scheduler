import { css } from 'styled-components';

const gridGap = 20;
const gridItemMaxWidth = 400;
const maxGridContainerWidth = 2 * gridItemMaxWidth + gridGap;
const ourPlansHeight = 32;

export const homepageCommonStyles = {
  introOurPlansHeight: `${ourPlansHeight}px`,
  introOurPlansBottom: `${-0.5 * ourPlansHeight}px`,
  gridGap: `${gridGap}px`,
  gridItemMaxWidth: `${gridItemMaxWidth}px`,
  maxGridContainerWidth: `${maxGridContainerWidth}px`,
};

export const homepageSectionStyles = css`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: var(--homepage__statistic__padding);
`;

export const homepageTitleStyles = css`
  font-size: 2rem;
  margin: 10px auto;
  text-align: center;
`;

export const homepageDetailsStyles = css`
  margin-top: 10px;
  margin-bottom: 20px;
  text-align: center;
  font-size: 1.2rem;
`;
