import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepageIntro from './HomepageIntro';

describe('<HomepageIntro />', () => {
  beforeEach(() => {
    render(<HomepageIntro />);
  });

  test('should have a title', () => {
    const title = `The most user-friendly tool to save time for Community Managers`;
    const titleElement = screen.getByText(title);
    expect(titleElement).toBeInTheDocument();
  });

  test('should have a description', () => {
    const description = `Social Media Scheduler helps you to standardize and post your ads in your favorite social networks`;
    const descriptionElement = screen.getByText(description);
    expect(descriptionElement).toBeInTheDocument();
  });

  test('should contain button that takes the user to the plans section', () => {
    const buttonElement = screen.getByText(`Our plans`);
    expect(buttonElement).toBeInTheDocument();
  });

  describe(`and the user clicks on the 'our plans' button`, () => {
    test('should take the user to the plans section', () => {
      const buttonElement = screen.getByText(`Our plans`);
      expect(buttonElement).toHaveAttribute('href', '#plans');
    });
  });
});
