import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { flexCenterContent } from '@/shared/styles/layout.styles';
import { homepageCommonStyles } from '../Homepage.styles';
import { homepageDetailsStyles, homepageSectionStyles, homepageTitleStyles } from '../Homepage.styles';

export const HomepageIntroContainer = styled.section`
  ${homepageSectionStyles}
  position: relative;
  min-height: 120px;
  color: ${globalColors.offWhite};
  background: linear-gradient(to left bottom, ${globalColors.warmBlack}, ${globalColors.orange});
`;

export const HomepageIntroTitle = styled.h1`
  ${homepageTitleStyles}
`;

export const HomepageIntroDescription = styled.p`
  ${homepageDetailsStyles}
`;

export const HomepageIntroPlans = styled.a`
  position: absolute;
  bottom: ${homepageCommonStyles.introOurPlansBottom};
  ${flexCenterContent}
  height: ${homepageCommonStyles.introOurPlansHeight};
  width: 60%;
  max-width: 200px;
  background-color: ${globalColors.offWhite};
  color: ${globalColors.warmBlack};
  text-decoration: none;
  font-size: 1.24rem;
  font-weight: bold;
  border-radius: 5px;
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);

  &:hover {
    color: ${globalColors.orange};
  }
`;
