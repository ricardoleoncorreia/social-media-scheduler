import React from 'react';
import { MdExpandMore } from 'react-icons/md';
import { HomepageIntroContainer, HomepageIntroDescription, HomepageIntroPlans, HomepageIntroTitle } from './HomepageIntro.styles';

export default function HomepageIntro(): JSX.Element {
  return (
    <HomepageIntroContainer data-testid='homepage-intro'>
      <HomepageIntroTitle>The most user-friendly tool to save time for Community Managers</HomepageIntroTitle>
      <HomepageIntroDescription>
        Social Media Scheduler helps you to standardize and post your ads in your favorite social networks
      </HomepageIntroDescription>
      <HomepageIntroPlans href='#plans'>
        Our plans
        <MdExpandMore size='28px' style={{ marginTop: `2.8px` }} />
      </HomepageIntroPlans>
    </HomepageIntroContainer>
  );
}
