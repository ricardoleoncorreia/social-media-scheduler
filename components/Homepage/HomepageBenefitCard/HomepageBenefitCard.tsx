import React from 'react';
import { IconType } from 'react-icons';
import { IHomepage } from '../Homepage.namespace';
import { HomepageBenefitCardIconsMap } from './HomepageBenefitCard.namespace';
import {
  HomepageBenefitCardContainer,
  HomepageBenefitCardDescription,
  HomepageBenefitCardIcon,
  HomepageBenefitCardTitle,
} from './HomepageBenefitCard.styles';

export default function HomepageBenefitCard(props: IHomepage.Props.BenefitCard): JSX.Element {
  const BenefitIcon: IconType = HomepageBenefitCardIconsMap[props.benefit.icon];

  return (
    <HomepageBenefitCardContainer key={props.benefit.id}>
      <HomepageBenefitCardIcon data-testid='benefit-icon'>
        <BenefitIcon size='30px' />
      </HomepageBenefitCardIcon>
      <HomepageBenefitCardTitle>{props.benefit.title}</HomepageBenefitCardTitle>
      <HomepageBenefitCardDescription>{props.benefit.description}</HomepageBenefitCardDescription>
    </HomepageBenefitCardContainer>
  );
}
