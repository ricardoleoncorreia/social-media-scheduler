import { MdDateRange, MdInsertChart, MdSchedule, MdTrendingUp } from 'react-icons/md';

export enum HomepageBenefitCardIcons {
  Schedule = 'Schedule',
  DateRange = 'DateRange',
  InsertChart = 'InsertChart',
  TrendingUp = 'TrendingUp',
}

export const HomepageBenefitCardIconsMap = {
  [HomepageBenefitCardIcons.DateRange]: MdDateRange,
  [HomepageBenefitCardIcons.Schedule]: MdSchedule,
  [HomepageBenefitCardIcons.InsertChart]: MdInsertChart,
  [HomepageBenefitCardIcons.TrendingUp]: MdTrendingUp,
};
