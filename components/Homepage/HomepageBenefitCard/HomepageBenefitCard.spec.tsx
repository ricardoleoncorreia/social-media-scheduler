import React from 'react';
import { render, screen } from '@testing-library/react';
import HomepageBenefitCard from './HomepageBenefitCard';
import { HomepageBenefitCardIcons } from './HomepageBenefitCard.namespace';
import { TestMocks } from '@/shared/mocks/test.mocks';

const benefit = {
  id: TestMocks.text.uuid,
  icon: HomepageBenefitCardIcons.TrendingUp,
  title: TestMocks.text.title,
  description: TestMocks.text.paragraph,
};

describe('<HomepageBenefitCard />', () => {
  beforeEach(() => {
    render(<HomepageBenefitCard benefit={benefit} />);
  });

  test(`should have a title ${benefit.title}`, () => {
    const titleElement = screen.getByText(benefit.title);
    expect(titleElement).toBeInTheDocument();
  });

  test('should have a description', () => {
    const descriptionElement = screen.getByText(benefit.description);
    expect(descriptionElement).toBeInTheDocument();
  });

  test('should have an icon', () => {
    const iconElement = screen.getByTestId(`benefit-icon`).querySelector('svg');
    expect(iconElement).toBeInTheDocument();
  });
});
