import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { homepageCommonStyles } from '../Homepage.styles';

export const HomepageBenefitCardContainer = styled.section`
  place-self: center;
  align-self: stretch;
  background-color: ${globalColors.warmBlack};
  padding: 20px;
  border-radius: 5px;
  min-width: 200px;
  max-width: ${homepageCommonStyles.gridItemMaxWidth};
  width: 90%;
`;

export const HomepageBenefitCardIcon = styled.span`
  color: ${globalColors.orange};
  font-size: 24px;
`;

export const HomepageBenefitCardTitle = styled.h3`
  color: ${globalColors.justWhite};
  font-weight: bold;
  margin: 6px auto;
`;

export const HomepageBenefitCardDescription = styled.p`
  color: ${globalColors.darkGrey};
`;
