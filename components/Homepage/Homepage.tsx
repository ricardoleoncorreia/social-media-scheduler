import React from 'react';
import HomepageIntro from './HomepageIntro/HomepageIntro';
import HomepageStatistics from './HomepageStatistics/HomepageStatistics';
import HomepageBenefits from './HomepageBenefits/HomepageBenefits';
import HomepageJoinToday from './HomepageJoinToday/HomepageJoinToday';
import HomepagePlans from './HomepagePlans/HomepagePlans';

export default function Homepage(): JSX.Element {
  return (
    <React.Fragment>
      <HomepageIntro />
      <HomepageStatistics />
      <HomepageBenefits />
      <HomepageJoinToday />
      <HomepagePlans />
    </React.Fragment>
  );
}
