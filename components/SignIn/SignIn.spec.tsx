import React from 'react';
import { render, screen } from '@testing-library/react';
import SignIn from './SignIn';

const PageNotImplementedMock = () => <div>PageNotImplementedMock</div>;
jest.mock('@/shared/components/PageNotImplemented/PageNotImplemented', () => PageNotImplementedMock);

describe('<SignIn />', () => {
  beforeEach(() => {
    render(<SignIn />);
  });

  test('should contains the work-in-progress message', () => {
    const wipMessage = screen.getByText('PageNotImplementedMock');
    expect(wipMessage).toBeInTheDocument();
  });
});
