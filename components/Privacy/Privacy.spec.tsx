import React from 'react';
import { render, screen } from '@testing-library/react';
import Privacy from './Privacy';

const policySections = [
  'Introduction',
  'Definitions',
  'Information Collection and Use',
  'Types of Data Collected',
  'Use of Data',
  'Retention of Data',
  'Transfer of Data',
  'Disclosure of Data',
  'Security of Data',
  'Your Data Protection Rights Under General Data Protection Regulation (GDPR)',
  'Your Data Protection Rights under the California Privacy Protection Act (CalOPPA)',
  'Your Data Protection Rights under the California Consumer Privacy Act (CCPA)',
  'Service Providers',
  'Analytics',
  'CI/CD tools',
  'Behavioral Remarketing',
  'Payments',
  'Links to Other Sites',
  'Children’s Privacy',
  'Changes to This Privacy Policy',
  'Contact Us',
];

describe('<Privacy />', () => {
  beforeEach(() => {
    render(<Privacy />);
  });

  test('should contain a title', () => {
    const title = screen.getByText('PRIVACY POLICY');
    expect(title).toBeInTheDocument();
  });

  policySections.forEach((policySection) => {
    test(`should contain a "${policySection}" section`, () => {
      const policy = screen.getByText(policySection);
      expect(policy).toBeInTheDocument();
    });
  });
});
