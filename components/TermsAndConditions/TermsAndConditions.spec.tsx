import React from 'react';
import { render, screen } from '@testing-library/react';
import TermsAndConditions from './TermsAndConditions';

const policySections = [
  'Introduction',
  'Communications',
  'Purchases',
  'Contests, Sweepstakes and Promotions',
  'Subscriptions',
  'Free Trial',
  'Fee Changes',
  'Refunds',
  'Content',
  'Prohibited Uses',
  'Analytics',
  'No Use By Minors',
  'Accounts',
  'Intellectual Property',
  'Copyright Policy',
  'DMCA Notice and Procedure for Copyright Infringement Claims',
  'Error Reporting and Feedback',
  'Links To Other Web Sites',
  'Disclaimer Of Warranty',
  'Limitation Of Liability',
  'Termination',
  'Governing Law',
  'Changes To Service',
  'Amendments To Terms',
  'Waiver And Severability',
  'Acknowledgement',
  'Contact Us',
];

describe('<TermsAndConditions />', () => {
  beforeEach(() => {
    render(<TermsAndConditions />);
  });

  test('should contain a title', () => {
    const title = screen.getByText('TERMS AND CONDITIONS');
    expect(title).toBeInTheDocument();
  });

  policySections.forEach((policySection) => {
    test(`should contain a "${policySection}" section`, () => {
      const policy = screen.getByText(policySection);
      expect(policy).toBeInTheDocument();
    });
  });
});
