import React from 'react';
import { render, screen } from '@testing-library/react';
import Plan from './Plan';

const PageNotImplementedMock = () => <div>PageNotImplementedMock</div>;
jest.mock('@/shared/components/PageNotImplemented/PageNotImplemented', () => PageNotImplementedMock);

describe('<Plan />', () => {
  beforeEach(() => {
    render(<Plan />);
  });

  test('should contains the work-in-progress message', () => {
    const wipMessage = screen.getByText('PageNotImplementedMock');
    expect(wipMessage).toBeInTheDocument();
  });
});
