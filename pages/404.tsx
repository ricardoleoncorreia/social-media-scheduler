import React from 'react';
import PageNotFound from '@/components/PageNotFound/PageNotFound';

export default function PageNotFound404(): JSX.Element {
  return <PageNotFound />;
}
