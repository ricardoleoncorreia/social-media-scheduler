import React from 'react';
import Homepage from '@/components/Homepage/Homepage';

export default function Index(): JSX.Element {
  return <Homepage />;
}
