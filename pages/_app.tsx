import { AppProps } from 'next/app';
import Head from 'next/head';
import { GlobalResponsiveVariables } from '@/core/styles/responsive.styles';
import { GlobalStyles } from '@/core/styles/global.styles';
import Header from '@/shared/components/Header/Header';
import Footer from '@/shared/components/Footer/Footer';
import { globalColors } from '@/core/styles/colors.styles';
import { addBasePath } from '@/shared/functions/addBasePath';

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Head>
        <link rel='icon' href={addBasePath('/favicon.ico')} />

        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name='theme-color' content={globalColors.orange} />
        <meta name='description' content='Improve your performance managing social networks' />

        <link rel='icon' type='image/png' sizes='32x32' href={addBasePath('/favicon32.png')} />
        <link rel='icon' type='image/png' sizes='16x16' href={addBasePath('/favicon16.png')} />
        <link rel='apple-touch-icon' sizes='180x180' href={addBasePath('/apple-touch-icon.png')} />
        <link rel='manifest' href={addBasePath('/manifest.json')} />

        <title>Social Media Scheduler</title>
      </Head>
      <GlobalResponsiveVariables />
      <GlobalStyles />
      <Header />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}
