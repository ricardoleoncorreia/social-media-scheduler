import React from 'react';
import TermsAndConditions from '@/components/TermsAndConditions/TermsAndConditions';

export default function TermsAndConditionsPage(): JSX.Element {
  return <TermsAndConditions />;
}
