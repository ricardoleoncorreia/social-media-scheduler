import styled from 'styled-components';

const spaceBetweenSections = `20px`;

export const PolicySection = styled.section`
  display: flex;
  flex-direction: column;
  margin: 48px var(--policy__container__h-padding);
  max-width: 760px;

  h2 {
    text-align: center;
    margin-bottom: ${spaceBetweenSections};
    font-size: 3rem;
  }

  h3 {
    margin: ${spaceBetweenSections} 0;
    font-size: 2rem;
  }

  p {
    margin: 10px 0;
    font-size: 1.2rem;
  }

  #disclaimer {
    margin-top: 5em;
    font-size: 0.7em;
  }
`;
