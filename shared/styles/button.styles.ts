import { css, FlattenSimpleInterpolation } from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { flexCenterContent } from './layout.styles';

export const buttonStylesBase: FlattenSimpleInterpolation = css`
  ${flexCenterContent}
  border: 2px solid ${globalColors.orange};
  border-radius: 6px;
  min-width: 120px;
  padding: 8px 12px;
  font-weight: bold;
  text-decoration: none;
`;

export const buttonStylesPrimary: FlattenSimpleInterpolation = css`
  background-color: ${globalColors.orange};
  color: ${globalColors.offWhite};
`;

export const buttonStylesGhost: FlattenSimpleInterpolation = css`
  background-color: ${globalColors.offWhite};
  color: ${globalColors.warmBlack};
`;
