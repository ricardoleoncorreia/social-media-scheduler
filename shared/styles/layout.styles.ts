import { css } from 'styled-components';

export const flexCenterContent = css`
  display: flex;
  justify-content: center;
  align-items: center;
`;
