import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IoIosConstruct } from 'react-icons/io';
import PlaceholderPage from './PlaceholderPage';
import { renderWithState } from '@/shared/testing/renderWithState';
import { PlaceHolderPageTestingProps } from './PlaceholderPage.props';

const runTests = ({ icon, message, pageName, path, isSignedIn }: PlaceHolderPageTestingProps) => {
  beforeEach(() => {
    renderWithState({
      isSignedIn,
      children: <PlaceholderPage icon={icon} message={message} />,
    });
  });

  test(`should show a title`, () => {
    const isText = typeof icon === 'string';
    const iconInTitle = isText ? screen.getByText(icon as string) : screen.getByTestId(`placeholder-page-icon`).querySelector('svg');
    expect(iconInTitle).toBeInTheDocument();
  });

  test(`should show a message`, () => {
    const messageElement = screen.getByText(message);
    expect(messageElement).toBeInTheDocument();
  });

  xdescribe('and user click on the button', () => {
    test(`should redirect to the ${pageName.toLowerCase()}`, () => {
      const button = screen.getByText(`Go to ${pageName}`);

      userEvent.click(button, { button: 0 });

      // expect(history.location.pathname).toBe(path);
    });
  });
};

// TODO: Implement cases that involve user events
describe('<PlaceholderPage />', () => {
  describe(`when the message is 'randomMessage'`, () => {
    const message = 'randomMessage';

    describe('and the icon is a text', () => {
      const icon = '404';

      describe('and user has signed in', () => {
        runTests({
          pageName: 'Dashboard',
          path: '/dashboard',
          isSignedIn: true,
          icon,
          message: message,
        });
      });

      describe('and user has signed out', () => {
        runTests({
          pageName: 'Homepage',
          path: '/',
          isSignedIn: false,
          icon,
          message: message,
        });
      });
    });

    describe('and the icon is a svg', () => {
      const icon = <IoIosConstruct size='12rem' />;

      describe('and user has signed in', () => {
        runTests({
          pageName: 'Dashboard',
          path: '/dashboard',
          isSignedIn: true,
          icon,
          message: message,
        });
      });

      describe('and user has signed out', () => {
        runTests({
          pageName: 'Homepage',
          path: '/',
          isSignedIn: false,
          icon,
          message: message,
        });
      });
    });
  });
});
