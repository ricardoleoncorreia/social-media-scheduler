import React from 'react';

export interface PlaceHolderPageProps {
  icon: string | React.ReactNode;
  message: string;
}

export interface PlaceHolderPageTestingBaseProps {
  path: string;
  pageName: string;
  isSignedIn: boolean;
}

export type PlaceHolderPageTestingProps = PlaceHolderPageProps & PlaceHolderPageTestingBaseProps;
