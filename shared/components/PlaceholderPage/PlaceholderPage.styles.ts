import styled from 'styled-components';
import { flexCenterContent } from '@/shared/styles/layout.styles';

export const PlaceholderPageContainer = styled.section`
  ${flexCenterContent}
  flex-direction: column;
  margin: 20px;
`;

export const PlaceholderTitle = styled.h2`
  font-size: 6rem;
`;

export const PlaceholderDetails = styled.p`
  font-size: 2rem;
  text-align: center;
  margin-bottom: 24px;
`;
