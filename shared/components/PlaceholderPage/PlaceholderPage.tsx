import React from 'react';
import { useGlobalState } from '@/core/state/state.context';
import { PrimaryLink } from '../RouterLinks/PrimaryLink';
import { RouterLink } from '../RouterLinks/RouterLink.props';
import { PlaceHolderPageProps } from './PlaceholderPage.props';
import { PlaceholderPageContainer, PlaceholderDetails, PlaceholderTitle } from './PlaceholderPage.styles';

export default function PlaceholderPage(props: PlaceHolderPageProps): JSX.Element {
  const [{ isSignedIn }] = useGlobalState();
  const linkProps: RouterLink.PrimaryLinkProps = {
    path: isSignedIn ? '/dashboard' : '/',
    content: `Go to ${isSignedIn ? 'Dashboard' : 'Homepage'}`,
  };

  return (
    <PlaceholderPageContainer>
      <PlaceholderTitle data-testid='placeholder-page-icon'>{props.icon}</PlaceholderTitle>
      <PlaceholderDetails>{props.message}</PlaceholderDetails>
      <PrimaryLink {...linkProps} />
    </PlaceholderPageContainer>
  );
}
