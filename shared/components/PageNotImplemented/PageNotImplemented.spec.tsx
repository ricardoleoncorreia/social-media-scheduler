import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PageNotImplemented from './PageNotImplemented';
import { PlaceHolderPageTestingBaseProps } from '../PlaceholderPage/PlaceholderPage.props';
import { renderWithState } from '@/shared/testing/renderWithState';

const runTests = ({ pageName, path, isSignedIn }: PlaceHolderPageTestingBaseProps) => {
  beforeEach(() => {
    renderWithState({
      isSignedIn,
      children: <PageNotImplemented />,
    });
  });

  test(`should show a icon`, () => {
    const icon = screen.getByTestId(`placeholder-page-icon`).querySelector('svg');
    expect(icon).toBeInTheDocument();
  });

  test(`should show a description`, () => {
    const description = screen.getByText(`We are currently working on this page`);
    expect(description).toBeInTheDocument();
  });

  xdescribe('and user click on the button', () => {
    test(`should redirect to the ${pageName.toLowerCase()}`, () => {
      const button = screen.getByText(`Go to ${pageName}`);

      userEvent.click(button, { button: 0 });

      // expect(history.location.pathname).toBe(path);
    });
  });
};

// TODO: Implement cases that involve user events
describe('<PageNotImplemented />', () => {
  describe('when user has signed in', () => {
    runTests({
      pageName: 'Dashboard',
      path: '/dashboard',
      isSignedIn: true,
    });
  });

  describe('when user has signed out', () => {
    runTests({
      pageName: 'Homepage',
      path: '/',
      isSignedIn: false,
    });
  });
});
