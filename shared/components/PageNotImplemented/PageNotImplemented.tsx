import React from 'react';
import { IoIosConstruct } from 'react-icons/io';
import PlaceholderPage from '../PlaceholderPage/PlaceholderPage';

export default function PageNotImplemented(): JSX.Element {
  const icon = <IoIosConstruct size='12rem' />;
  const message = 'We are currently working on this page';
  return <PlaceholderPage icon={icon} message={message} />;
}
