import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';

const textMargin = `4px`;

export const FooterContainer = styled.footer`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: var(--footer__container__flex-direction);
  padding: 20px;
  background-color: ${globalColors.orange};
  color: ${globalColors.offWhite};
`;

export const FooterLegalContainer = styled.section`
  display: flex;
  align-items: var(--footer__legal__align-items);
  flex-direction: column;

  a {
    margin: ${textMargin};
  }
`;

export const FooterRights = styled.p`
  display: flex;
  align-items: var(--footer__rights__align-items);
  flex-direction: column;
  text-align: center;
`;

export const FooterRightsText = styled.span`
  margin: ${textMargin};
`;
