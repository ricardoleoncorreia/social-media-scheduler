import React from 'react';
import { CustomStyleLink } from '../RouterLinks/CustomStyleLink';
import { FooterContainer, FooterLegalContainer, FooterRights, FooterRightsText } from './Footer.styles';

export default function Footer(): JSX.Element {
  const currentYear = new Date().getFullYear();

  return (
    <FooterContainer>
      <FooterLegalContainer>
        <CustomStyleLink path='/terms-and-conditions' content='Terms and Conditions' offWhite={true} underline={true} />
        <CustomStyleLink path='/privacy' content='Privacy' offWhite={true} underline={true} />
      </FooterLegalContainer>
      <FooterRights>
        <FooterRightsText>&copy;{currentYear} Ricardo León Correia</FooterRightsText>
        <FooterRightsText>All Rights Reserved</FooterRightsText>
      </FooterRights>
    </FooterContainer>
  );
}
