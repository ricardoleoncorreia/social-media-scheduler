import React from 'react';
import { screen, render } from '@testing-library/react';
import Footer from './Footer';
import userEvent from '@testing-library/user-event';

// TODO: Implement cases that involve user events
describe('<Footer />', () => {
  beforeEach(() => {
    render(<Footer />);
  });

  test('should contain a terms and conditions link', () => {
    const terms = screen.getByText(/terms and conditions/i);
    expect(terms).toBeInTheDocument();
  });

  xdescribe('when the user clicks on the terms and conditions link', () => {
    test('should take the user to the terms and conditions page', () => {
      const button = screen.getByText(/terms and conditions/i);

      userEvent.click(button);

      // expect(history.location.pathname).toBe('/terms-and-conditions');
    });
  });

  test('should contain a privacy link', () => {
    const privacy = screen.getByText(/privacy/i);
    expect(privacy).toBeInTheDocument();
  });

  xdescribe('when the user clicks on the privacy link', () => {
    test('should take the user to the privacy page', () => {
      const button = screen.getByText(/privacy/i);

      userEvent.click(button);

      // expect(history.location.pathname).toBe('/privacy');
    });
  });

  test('should update copyright year to current year', () => {
    const currentYear = new Date().getFullYear().toString();
    const yearRegex = new RegExp(currentYear, 'i');
    const yearText = screen.getByText(yearRegex);
    expect(yearText).toBeInTheDocument();
  });

  test('should have a rights reserved text', () => {
    const privacy = screen.getByText(/all rights reserved/i);
    expect(privacy).toBeInTheDocument();
  });
});
