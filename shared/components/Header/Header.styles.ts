import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';

const headerHeight = 60;

export const HeaderContainer = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: ${headerHeight}px;
  padding: 0 24px;
  background-color: ${globalColors.warmBlack};
`;

export const HeaderBrandLogo = styled.img`
  height: ${headerHeight}px;
`;
