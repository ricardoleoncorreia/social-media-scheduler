import React from 'react';
import HeaderMenu from './HeaderMenu/HeaderMenu';
import { HeaderBrandLogo, HeaderContainer } from './Header.styles';
import { useGlobalState } from '@/core/state/state.context';
import { CustomStyleLink } from '../RouterLinks/CustomStyleLink';
import { addBasePath } from '@/shared/functions/addBasePath';

export default function Header(): JSX.Element {
  const [{ isSignedIn }] = useGlobalState();

  const mainRoute = isSignedIn ? '/dashboard' : '/';
  const content = <HeaderBrandLogo src={addBasePath('/static/img/brand-logo.png')} alt='Brand Logo' />;

  return (
    <HeaderContainer data-testid='header-container'>
      <CustomStyleLink path={mainRoute} content={content} />
      <HeaderMenu />
    </HeaderContainer>
  );
}
