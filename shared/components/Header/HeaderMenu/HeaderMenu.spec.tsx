import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import HeaderMenu from './HeaderMenu';
import { TestMocks } from '@/shared/mocks/test.mocks';
import { renderWithState } from '@/shared/testing/renderWithState';

const username = TestMocks.state.username;

// TODO: Implement cases that involve user events
describe('<HeaderMenu />', () => {
  describe(`when the username is "${username}"`, () => {
    const usernameRegex = new RegExp(username, 'i');

    describe('and the user is logged in', () => {
      beforeEach(() => {
        renderWithState({
          isSignedIn: true,
          children: <HeaderMenu />,
        });
      });

      test('should show the sign out icon', () => {
        const signOutIcon = screen.getByTitle(/sign out/i);
        expect(signOutIcon).toBeInTheDocument();
      });

      test('should NOT show the sign in icon', () => {
        const signInIcon = screen.queryByTitle(/sign in/i);
        expect(signInIcon).toBeNull();
      });

      test('should show the username', () => {
        const username = screen.getByTitle(usernameRegex);
        expect(username).toBeInTheDocument();
      });

      xdescribe('and user clicks on the sign out icon', () => {
        test('should log out', () => {
          const brandLink = screen.getByTestId('header-menu-container').querySelector('a');

          userEvent.click(brandLink as HTMLAnchorElement, { button: 0 });

          // expect(history.location.pathname).toBe('/');
        });
      });
    });

    describe('and the user is NOT logged in', () => {
      beforeEach(() => {
        renderWithState({
          isSignedIn: false,
          children: <HeaderMenu />,
        });
      });

      test('should NOT show the sign out icon', () => {
        const signOutIcon = screen.queryByTitle(/sign out/i);
        expect(signOutIcon).toBeNull();
      });

      test('should show the sign in icon', () => {
        const signInIcon = screen.getByTitle(/sign in/i);
        expect(signInIcon).toBeInTheDocument();
      });

      test('should NOT show the username', () => {
        const username = screen.queryByTitle(usernameRegex);
        expect(username).toBeNull();
      });

      xdescribe('and user clicks on the sign in icon', () => {
        test('should redirect to the sign in page', () => {
          const brandLink = screen.getByTestId('header-menu-container').querySelector('a');

          userEvent.click(brandLink as HTMLAnchorElement, { button: 0 });

          // expect(history.location.pathname).toBe('/sign-in');
        });
      });
    });
  });
});
