import styled from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';

export const HeaderMenuContainer = styled.section`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  color: ${globalColors.orange};
`;

export const HeaderMenuNickname = styled.p`
  max-width: 32vw;
  margin-bottom: 0;
  margin-right: 10px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;
