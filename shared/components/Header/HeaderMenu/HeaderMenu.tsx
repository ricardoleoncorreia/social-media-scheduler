import React from 'react';
import { FaSignInAlt, FaSignOutAlt } from 'react-icons/fa';
import { useGlobalState } from '@/core/state/state.context';
import { CustomStyleLink } from '@/shared/components/RouterLinks/CustomStyleLink';
import { HeaderMenuContainer, HeaderMenuNickname } from './HeaderMenu.styles';

export default function HeaderMenu(): JSX.Element {
  const [{ isSignedIn, username }] = useGlobalState();

  const path = isSignedIn ? '/' : '/sign-in';
  const iconTitle = isSignedIn ? 'Sign Out' : 'Sign In';
  const Icon = isSignedIn ? FaSignOutAlt : FaSignInAlt;
  const content = <Icon title={iconTitle} size='20px' />;

  return (
    <HeaderMenuContainer data-testid='header-menu-container'>
      {username && <HeaderMenuNickname title={username}>{username}</HeaderMenuNickname>}
      <CustomStyleLink path={path} content={content} orange={true} />
    </HeaderMenuContainer>
  );
}
