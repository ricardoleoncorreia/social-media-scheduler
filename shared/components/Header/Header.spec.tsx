import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Header from './Header';
import { renderWithState } from '@/shared/testing/renderWithState';

jest.mock('./HeaderMenu/HeaderMenu.tsx', () => () => <div>HeaderMenu</div>);

const runCommonTests = () => {
  test('should contain a menu', () => {
    const menu = screen.getByText('HeaderMenu');
    expect(menu).toBeInTheDocument();
  });

  test('should contain a brand logo', () => {
    const brandLogo = screen.getByAltText(/brand logo/i);
    expect(brandLogo).toBeInTheDocument();
  });
};

// TODO: Implement cases that involve user events
describe('<Header />', () => {
  describe('when the user is NOT logged in', () => {
    beforeEach(() => {
      renderWithState({
        isSignedIn: false,
        children: <Header />,
      });
    });

    runCommonTests();

    xdescribe('and the user clicks on the brand logo', () => {
      test('should redirect to the homepage', () => {
        const brandLink = screen.getByTestId('header-container').querySelector('a');

        userEvent.click(brandLink as HTMLAnchorElement, { button: 0 });

        // expect(history.location.pathname).toBe('/');
      });
    });
  });

  describe('when the user is logged in', () => {
    beforeEach(() => {
      renderWithState({
        isSignedIn: true,
        children: <Header />,
      });
    });

    runCommonTests();

    xdescribe('and the user clicks on the brand logo', () => {
      test('should redirect to the dashboard', () => {
        const brandLink = screen.getByTestId('header-container').querySelector('a');

        userEvent.click(brandLink as HTMLAnchorElement, { button: 0 });

        // expect(history.location.pathname).toBe('/dashboard');
      });
    });
  });
});
