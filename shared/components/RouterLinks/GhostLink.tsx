import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { buttonStylesBase, buttonStylesPrimary, buttonStylesGhost } from '@/shared/styles/button.styles';
import { RouterLink } from './RouterLink.props';

export const GhostLinkContainer = styled.div`
  a {
    ${buttonStylesBase}
    ${buttonStylesGhost}

    &:hover {
      ${buttonStylesPrimary}
    }
  }
`;

export const GhostLink = (props: RouterLink.Base.Props): JSX.Element => (
  <GhostLinkContainer data-testid='ghost-link-container'>
    <Link href={props.path}>{props.content}</Link>
  </GhostLinkContainer>
);
