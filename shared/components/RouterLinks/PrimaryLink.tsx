import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { buttonStylesBase, buttonStylesPrimary, buttonStylesGhost } from '@/shared/styles/button.styles';
import { RouterLink } from './RouterLink.props';

export const PrimaryLinkContainer = styled.div`
  a {
    ${buttonStylesBase}
    ${buttonStylesPrimary}

    &:hover {
      ${buttonStylesGhost}
    }
  }
`;

export const PrimaryLink = (props: RouterLink.Base.Props): JSX.Element => (
  <PrimaryLinkContainer data-testid='primary-link-container'>
    <Link href={props.path}>{props.content}</Link>
  </PrimaryLinkContainer>
);
