import React from 'react';
import Link from 'next/link';
import styled, { css, FlattenSimpleInterpolation } from 'styled-components';
import { globalColors } from '@/core/styles/colors.styles';
import { flexCenterContent } from '@/shared/styles/layout.styles';
import { RouterLink } from './RouterLink.props';

const orangeTheme: FlattenSimpleInterpolation = css`
  color: ${globalColors.orange};

  &:hover {
    color: ${globalColors.offWhite};
  }
`;

const offWhiteTheme: FlattenSimpleInterpolation = css`
  color: ${globalColors.offWhite};
`;

const underlineTheme: FlattenSimpleInterpolation = css`
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`;

export const CustomStyleLinkContainer = styled.div<RouterLink.CustomStyle.Theme>`
  a {
    cursor: pointer;
    ${flexCenterContent}
    ${(props: RouterLink.CustomStyle.Theme) => !!props.orange && orangeTheme}
    ${(props: RouterLink.CustomStyle.Theme) => !!props.offWhite && offWhiteTheme}
    ${(props: RouterLink.CustomStyle.Theme) => !!props.underline && underlineTheme}
  }
`;

export const CustomStyleLink = (props: RouterLink.CustomStyleLinkProps): JSX.Element => (
  <CustomStyleLinkContainer orange={props.orange} offWhite={props.offWhite} underline={props.underline}>
    <Link href={props.path}>
      <CustomLinkContent>{props.content}</CustomLinkContent>
    </Link>
  </CustomStyleLinkContainer>
);

const CustomLinkContent = React.forwardRef<any, any>(({ onClick, href, children }, ref) => {
  return (
    <a href={href} onClick={onClick} ref={ref}>
      {children}
    </a>
  );
});
