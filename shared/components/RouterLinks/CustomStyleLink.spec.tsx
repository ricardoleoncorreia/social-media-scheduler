import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { CustomStyleLink } from './CustomStyleLink';
import { globalColors } from '@/core/styles/colors.styles';
import { TestMocks } from '@/shared/mocks/test.mocks';

const mockedPathname = TestMocks.router.route;
const buttonText = TestMocks.text.button;

// TODO: need to test the hover properties
describe('<CustomStyleLink />', () => {
  describe(`when path is "${mockedPathname}"`, () => {
    describe(`and content is "${buttonText}"`, () => {
      test('should have a link with the content', () => {
        render(<CustomStyleLink path={mockedPathname} content={buttonText} />);

        const button = screen.getByText(buttonText);
        expect(button).toBeInTheDocument();
      });

      xdescribe('and user clicks on the link', () => {
        beforeEach(() => {
          render(<CustomStyleLink path={mockedPathname} content={buttonText} />);
        });

        test(`should redirect to "${mockedPathname}"`, () => {
          const button = screen.getByText(buttonText);

          userEvent.click(button, { button: 0 });

          // expect(history.location.pathname).toBe(mockedPathname);
        });
      });

      describe(`and "orange" style is requested`, () => {
        beforeEach(() => {
          render(<CustomStyleLink path={mockedPathname} content={buttonText} orange={true} />);
        });

        test(`should color the link content to orange`, () => {
          const link = screen.getByText(buttonText);
          expect(link).toHaveStyle(`color: ${globalColors.orange}`);
        });
      });

      describe(`and "white" style is requested`, () => {
        beforeEach(() => {
          render(<CustomStyleLink path={mockedPathname} content={buttonText} offWhite={true} />);
        });

        test(`should color the link content to white`, () => {
          const link = screen.getByText(buttonText);
          expect(link).toHaveStyle(`color: ${globalColors.offWhite}`);
        });
      });

      describe(`and "underline" style is requested`, () => {
        beforeEach(() => {
          render(<CustomStyleLink path={mockedPathname} content={buttonText} underline={true} />);
        });

        test(`should remove the underline on the unhover state`, () => {
          const link = screen.getByText(buttonText);
          expect(link).toHaveStyle(`text-decoration: none`);
        });
      });
    });
  });
});
