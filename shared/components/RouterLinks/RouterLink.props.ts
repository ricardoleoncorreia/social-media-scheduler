import React from 'react';

export declare namespace RouterLink {
  export namespace Base {
    export interface Props {
      path: string;
      content: string | React.ReactNode;
    }
  }

  export namespace CustomStyle {
    export interface Theme {
      orange?: boolean;
      offWhite?: boolean;
      underline?: boolean;
    }
  }

  export type PrimaryLinkProps = Base.Props;
  export type CustomStyleLinkProps = CustomStyle.Theme & Base.Props;
}
