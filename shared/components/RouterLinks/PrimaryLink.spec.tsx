import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { PrimaryLink } from './PrimaryLink';
import { globalColors } from '@/core/styles/colors.styles';
import { TestMocks } from '@/shared/mocks/test.mocks';

const mockedPathname = TestMocks.router.route;
const buttonText = TestMocks.text.button;

// TODO: need to test the hover properties
// TODO: Implement cases that involve user events
describe('<PrimaryLink />', () => {
  describe(`when path is "${mockedPathname}"`, () => {
    describe(`and content is "${buttonText}"`, () => {
      beforeEach(() => {
        render(<PrimaryLink path={mockedPathname} content={buttonText} />);
      });

      test('should have a link with the content', () => {
        const button = screen.getByText(buttonText);
        expect(button).toBeInTheDocument();
      });

      test('should have a primary background color', () => {
        const button = screen.getByText(buttonText);
        expect(button).toHaveStyle(`background-color: ${globalColors.orange}`);
      });

      test('should have a white font color', () => {
        const button = screen.getByText(buttonText);
        expect(button).toHaveStyle(`color: ${globalColors.offWhite}`);
      });

      test('should have a border with the primary color', () => {
        const button = screen.getByText(buttonText);
        expect(button).toHaveStyle(`border: 2px solid ${globalColors.orange}`);
      });

      xdescribe('and user clicks on the link', () => {
        test(`should redirect to "${mockedPathname}"`, () => {
          const button = screen.getByText(buttonText);

          userEvent.click(button, { button: 0 });

          // expect(history.location.pathname).toBe(mockedPathname);
        });
      });
    });
  });
});
