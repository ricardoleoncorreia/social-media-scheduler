import { HomepageBenefitCardIcons } from '@/components/Homepage/HomepageBenefitCard/HomepageBenefitCard.namespace';

/**
 * Once the corresponding feature are implemented, this
 * class will be deprecated.
 */
export class Constants {
  static readonly isLoggedIn = false;

  static readonly header = {
    username: 'Ricardo León',
  };

  static readonly homepage = {
    statistics: [
      { id: 'social-networks', label: 'Social Networks', value: 2 },
      { id: 'posts', label: 'Posts', value: 31200 },
      { id: 'members', label: 'Members', value: 22 },
    ],
    benefits: [
      {
        id: 'most-popular-networks',
        icon: HomepageBenefitCardIcons.TrendingUp,
        title: 'Most popular social networks',
        description: 'We provide connection with the most popular social networks so you can stay in contact with your followers.',
      },
      {
        id: 'create-schedule',
        icon: HomepageBenefitCardIcons.DateRange,
        title: 'Create and schedule',
        description: 'An easy-to-use system. With only two steps, anyone can post an advertisement: create and schedule.',
      },
      {
        id: 'real-time',
        icon: HomepageBenefitCardIcons.Schedule,
        title: 'Real Time',
        description: 'Once the time you selected has come, the system will post your advertisement with the content you added to it',
      },
      {
        id: 'statistics',
        icon: HomepageBenefitCardIcons.InsertChart,
        title: 'Statistics',
        description: 'Monitor your activity in the system at any time. You can see the details of your posts and more!',
      },
    ],
    plans: [
      { id: 'free', title: 'Free', amount: 0, description: 'Access to all free features' },
      {
        id: 'community-manager',
        title: 'Community Manager',
        amount: 19,
        description: 'Manage the accounts of your top clients',
        isRecommended: true,
      },
      { id: 'enterprise', title: 'Enterprise', amount: 99, description: 'Get access to the premium features' },
    ],
  };
}
