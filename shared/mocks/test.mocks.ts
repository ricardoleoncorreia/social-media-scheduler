import { IStateContext } from '@/core/state/state.namespace';

interface ISignState {
  isSignedIn: boolean;
  username?: string;
}

interface ITestMocks {
  state: {
    username: string;
    signedInState: ISignState;
    signedOutState: ISignState;
    stateReducer: () => IStateContext.State;
  };
  router: {
    route: string;
  };
  text: {
    button: string;
    title: string;
    paragraph: string;
    uuid: string;
  };
}

const username = 'mockedUsername';
const route = '/mock/route';

export const TestMocks: ITestMocks = {
  state: {
    username,
    signedInState: { isSignedIn: true, username },
    signedOutState: { isSignedIn: false, username: undefined },
    stateReducer: () => {
      return {} as IStateContext.State;
    },
  },
  router: {
    route,
  },
  text: {
    button: 'Click Me',
    title: 'Mocked Title',
    paragraph: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam aliquet turpis sit amet condimentum sollicitudin.`,
    uuid: '5921c8d6-ecca-11eb-9a03-0242ac130003',
  },
};
