import React, { ReactNode } from 'react';
import { render, RenderResult } from '@testing-library/react';
import { GlobalStateProvider } from '@/core/state/state.context';
import { TestMocks } from '../mocks/test.mocks';

interface RenderWithStateProps {
  isSignedIn: boolean;
  children: ReactNode;
}

export const renderWithState = ({ isSignedIn, children }: RenderWithStateProps): RenderResult => {
  const { signedInState, signedOutState, stateReducer } = TestMocks.state;
  const state = isSignedIn ? signedInState : signedOutState;

  return render(
    <GlobalStateProvider initialState={state} reducer={stateReducer}>
      {children}
    </GlobalStateProvider>,
  );
};
