/**
 * TODO: this method was built to handle `src` image attributes.
 * Monitor if in future releases of NextJS framework this changes.
 * @param path public asset url.
 * @returns full url with base path.
 */
export function addBasePath(path?: string): string {
  return `${process.env.NEXT_PUBLIC_BASE_PATH_GITLAB || ''}${path}`;
}
